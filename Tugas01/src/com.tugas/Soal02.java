package com.tugas;

import java.util.Scanner;

public class Soal02 {
	
	public static void main (String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.print("Masukkan Angka : ");
		
		int n = scn.nextInt();
		
		for (int i=0;i<n*2;i++ ) {
			if (i%2 != 0) {
				System.out.print(i+"\t");
			}
		}
		System.out.print("\n");
	}
}