package com.tugas;

import java.util.Scanner;

public class Soal05 {
	
	public static void main (String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.print("Masukkan Angka : ");
		
		int n = scn.nextInt();
		int a = 1;
		int b = 1;
		int c = 1;

		for (int i=0;i<n;i++ ) {
			System.out.print(a+"\t");				
			
			int d = a + b + c;
			a = b;
			b = c;
			c = d;
		}
		System.out.print("\n");
	}
}