package com.tugas;

import java.util.Scanner;

public class Soal06 {
	
	public static void main (String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.print("Masukkan Angka : ");
		
		int n = scn.nextInt();
		int a = 0;

		for (int i=0;i<n*3;i++ ) {
			int b = 0;

			for (int c=i;c>=1;c--){

				if (i%c == 0) {
					b = b+1;	
				}
			}
			
			if (b == 2) {
				System.out.print(i+"\t");				
			}
		}
		System.out.print("\n");
	}
}