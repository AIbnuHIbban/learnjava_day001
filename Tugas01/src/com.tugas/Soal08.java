package com.tugas;

import java.util.Scanner;

public class Soal08 {
	
	public static void main (String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.print("Masukkan Angka : ");
		
		int n = scn.nextInt();

		String[] huruf = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

		for (int i=0;i<=n;i++ ) {
			if (i == 0) {
				System.out.print(huruf[i]+"\t");
			}else if(i%2 == 0){
				if(i > 0){
					System.out.print(i+"\t");
				}
				System.out.print(huruf[i]+"\t");
			}
		}
		System.out.print("\n");
	}

}