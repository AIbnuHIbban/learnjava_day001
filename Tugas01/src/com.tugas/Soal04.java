package com.tugas;

import java.util.Scanner;

public class Soal04 {
	
	public static void main (String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.print("Masukkan Angka : ");
		
		int n = scn.nextInt();
		int a = 0;
		int b = 1;
		
		for (int i=0;i<=n;i++ ) {
			
			if (a > 0) {
				System.out.print(a+"\t");				
			}
			
			int c = a + b;
			a = b;
			b = c;
		}
		System.out.print("\n");
	}
}
